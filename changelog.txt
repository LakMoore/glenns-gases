Version 1.6.6 (Patch)

Additions:
- Added Whispering Fog, a very rare, invisible gas that generates near bedrock. Credit for idea goes to Pakratt.
- Added Cracked Bedrock (for Whispering Fog).

Bugfixes:
- Fix bedrock not spawning void gas.
- Fix configurations creating error messages for string arrays.
