package glenn.gases.server;

import glenn.gases.common.CommonProxy;

/** Server-side proxy object. */
public class ServerProxy extends CommonProxy
{
	/** Registers client-side event handlers. */
	public void registerEventHandlers()
	{
		super.registerEventHandlers();
	}
}
