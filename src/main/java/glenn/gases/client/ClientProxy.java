package glenn.gases.client;

import glenn.gases.Gases;
import glenn.gases.client.render.RenderFX;
import glenn.gases.client.render.RenderGlowstoneShard;
import glenn.gases.client.render.RenderSmallLightning;
import glenn.gases.common.CommonProxy;
import glenn.gases.common.entity.EntityFX;
import glenn.gases.common.entity.EntityGlowstoneShard;
import glenn.gases.common.entity.EntitySmallLightning;
import glenn.gases.common.entity.EntitySnapdragon;
import glenn.moddingutils.UpdateChecker;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;

/** Client-side proxy object. */
public class ClientProxy extends CommonProxy
{
	/** Registers client-side renderers (entities) */
	@Override
	public void registerRenderers()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntitySmallLightning.class, new RenderSmallLightning());
		RenderingRegistry.registerEntityRenderingHandler(EntityGlowstoneShard.class, new RenderGlowstoneShard());
		RenderingRegistry.registerEntityRenderingHandler(EntityFX.class, new RenderFX());
		RenderingRegistry.registerEntityRenderingHandler(EntitySnapdragon.class, new RenderSnowball(Gases.items.snapdragon, 0));
	}

	/** Registers client-side event handlers. */
	public void registerEventHandlers()
	{
		super.registerEventHandlers();
		FMLCommonHandler.instance().bus().register(new FMLClientEvents());

		if (Gases.configurations.updateChecker.enable)
		{
			MinecraftForge.EVENT_BUS.register(new UpdateChecker("https://www.jamieswhiteshirt.com/trackable/gases.php", "Glenn's Gases", Gases.MODID, Gases.VERSION, Gases.TARGETVERSION));
		}
	}

	@Override
	public <REQ extends IMessage, REPLY extends IMessage> void registerMessage(Class<? extends IMessageHandler<REQ, REPLY>> messageHandler, Class<REQ> requestMessageType, int discriminator)
	{
		super.registerMessage(messageHandler, requestMessageType, discriminator);
		Gases.networkWrapper.registerMessage(messageHandler, requestMessageType, discriminator, Side.CLIENT);
	}

	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx)
	{
		return ctx.side == Side.CLIENT ? Minecraft.getMinecraft().thePlayer : super.getPlayerEntity(ctx);
	}
}
