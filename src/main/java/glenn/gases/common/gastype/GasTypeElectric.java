package glenn.gases.common.gastype;

import glenn.gases.common.entity.EntitySmallLightning;
import glenn.gasesframework.api.Combustibility;
import glenn.gasesframework.api.gastype.GasType;

import java.util.List;
import java.util.Random;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class GasTypeElectric extends GasType
{
	public GasTypeElectric(boolean isIndustrial, int gasIndex, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasIndex, name, color, opacity, density, combustibility);
	}

	/**
	 * Called before a gas block of this type ticks.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param random
	 */
	public void preTick(World world, int x, int y, int z, Random random)
	{
		if (!world.isRemote && random.nextInt(8) == 0)
		{
			List nearbyEntities = world.getEntitiesWithinAABB(EntityLivingBase.class, AxisAlignedBB.getBoundingBox(x - 16.0D, y - 16.0D, z - 16.0D, x + 17.0D, y + 17.0D, z + 17.0D));

			if (nearbyEntities.size() > 0)
			{
				int metadata = world.getBlockMetadata(x, y, z);
				double yOffset = (getMinY((IBlockAccess) world, x, y, z, metadata) + getMaxY((IBlockAccess) world, x, y, z, metadata)) / 2.0D;
				world.spawnEntityInWorld(new EntitySmallLightning(world, x + 0.5D, y + yOffset, z + 0.5D));
			}
		}
	}
}