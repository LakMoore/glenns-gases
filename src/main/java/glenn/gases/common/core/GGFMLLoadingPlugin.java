package glenn.gases.common.core;

import java.util.Map;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin.TransformerExclusions;

@IFMLLoadingPlugin.Name(value = "Glenn's Gases Core")
@IFMLLoadingPlugin.MCVersion(value = "1.7.10")
@TransformerExclusions(value = "glenn.gases.common.core")
public class GGFMLLoadingPlugin implements IFMLLoadingPlugin
{
	@Override
	public String[] getASMTransformerClass()
	{
		return new String[] { GGClassTransformer.class.getName() };
	}

	@Override
	public String getModContainerClass()
	{
		return DummyContainerGases.class.getName();
	}

	@Override
	public String getSetupClass()
	{
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data)
	{
	}

	@Override
	public String getAccessTransformerClass()
	{
		return null;
	}
}