package glenn.gases.common.item;

import glenn.gases.common.entity.EntityBlueDust;
import glenn.gases.common.entity.EntityDust;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class ItemBlueDust extends ItemDust
{
	@Override
	public EntityDust getThrownEntity(World world, EntityPlayer player)
	{
		return new EntityBlueDust(world, player);
	}
}
