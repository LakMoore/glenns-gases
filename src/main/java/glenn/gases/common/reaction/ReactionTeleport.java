package glenn.gases.common.reaction;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Random;
import java.util.Set;

import glenn.gasesframework.api.reaction.EntityReaction;
import glenn.gasesframework.api.reaction.environment.IEntityReactionEnvironment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityFireworkRocket;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.util.AxisAlignedBB;

public class ReactionTeleport extends EntityReaction
{
	public final double radius;
	public final int maxSearches;

	private final Set<Class<?>> typeFilter = Collections.newSetFromMap(new IdentityHashMap<Class<?>, Boolean>());

	public ReactionTeleport(double radius, int maxSearches)
	{
		this.radius = radius;
		this.maxSearches = maxSearches;

		typeFilter.add(EntityLivingBase.class);
		typeFilter.add(EntityItem.class);
		typeFilter.add(IProjectile.class);
		typeFilter.add(EntityBoat.class);
		typeFilter.add(EntityMinecart.class);
		typeFilter.add(EntityTNTPrimed.class);
		typeFilter.add(EntityFireworkRocket.class);
		typeFilter.add(EntityXPOrb.class);
	}

	private boolean isValidEntityClass(Class<?> clazz)
	{
		for (Class<?> clazz1 : typeFilter)
		{
			if (clazz1.isAssignableFrom(clazz))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void react(IEntityReactionEnvironment environment)
	{
		Entity entity = environment.getB();
		Random random = environment.getRandom();
		if (isValidEntityClass(entity.getClass()) && random.nextInt(4) == 0)
		{
			for (int i = 0; i < maxSearches; i++)
			{
				double x = (random.nextDouble() - random.nextDouble()) * radius;
				double y = (random.nextDouble() - random.nextDouble()) * radius;
				double z = (random.nextDouble() - random.nextDouble()) * radius;
				AxisAlignedBB aabb = entity.boundingBox.copy().offset(x, y, z);
				if (entity.worldObj.getCollidingBoundingBoxes(entity, aabb).isEmpty())
				{
					entity.playSound("minecraft:mob.endermen.portal", 1.0f, 1.0f);
					entity.setPosition(entity.posX + x, entity.posY + y, entity.posZ + z);
					entity.motionX = 0.0D;
					entity.motionY = 0.0D;
					entity.motionZ = 0.0D;
					entity.rotationPitch = random.nextFloat() * 180.0f - 90.0f;
					entity.rotationYaw = random.nextFloat() * 360.0f;
					entity.playSound("minecraft:mob.endermen.portal", 1.0f, 1.0f);
					break;
				}
			}
		}
	}
}
