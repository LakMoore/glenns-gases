package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.reaction.BlockReaction;
import glenn.gasesframework.api.reaction.environment.IBlockReactionEnvironment;
import net.minecraft.block.Block;

public class ReactionRustBlocks extends BlockReaction
{
	@Override
	public void react(IBlockReactionEnvironment environment)
	{
		Block b = environment.getB();
		Block replacement = Gases.registry.getRustedBlock(b);
		if (replacement != null)
		{
			environment.setB(replacement);
		}
	}
}