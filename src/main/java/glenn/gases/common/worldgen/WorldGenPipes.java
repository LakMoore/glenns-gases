package glenn.gases.common.worldgen;

import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.pipetype.PipeType;

import java.util.*;

import glenn.moddingutils.KeyVec;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.util.ForgeDirection;

public class WorldGenPipes extends WorldGenerator
{
	protected static class PipeHead
	{
		private static KeyVec[] sideVecs = new KeyVec[6];
		private static int[] sideSupport = new int[6];
		private static boolean[] openSides = new boolean[6];
		private static int[] sideChances = new int[6];
		public final KeyVec pos;
		public final ForgeDirection initialDirection;

		public PipeHead(KeyVec pos, ForgeDirection initialDirection)
		{
			this.pos = pos;
			this.initialDirection = initialDirection;
		}

		public void branch(World world, Random random, HashSet<KeyVec> placements, HashMap<KeyVec, PipeHead> heads)
		{
			for (ForgeDirection direction : ForgeDirection.VALID_DIRECTIONS)
			{
				int ordinal = direction.ordinal();
				KeyVec keyVec = new KeyVec(pos.x + direction.offsetX, pos.y + direction.offsetY, pos.z + direction.offsetZ);
				sideVecs[ordinal] = keyVec;

				Block block = world.getBlock(keyVec.x, keyVec.y, keyVec.z);
				boolean isPipe = placements.contains(keyVec);
				sideSupport[ordinal] = block.isOpaqueCube() ? 1 : (isPipe ? -2 : 0);
				openSides[ordinal] = block == Blocks.air && !isPipe;
			}

			sideChances[0] = sideSupport[1] + sideSupport[2] + sideSupport[3] + sideSupport[4] + sideSupport[5];
			sideChances[1] = sideSupport[0] + sideSupport[2] + sideSupport[3] + sideSupport[4] + sideSupport[5];
			sideChances[2] = sideSupport[3] + sideSupport[0] + sideSupport[1] + sideSupport[4] + sideSupport[5];
			sideChances[3] = sideSupport[2] + sideSupport[0] + sideSupport[1] + sideSupport[4] + sideSupport[5];
			sideChances[4] = sideSupport[5] + sideSupport[0] + sideSupport[1] + sideSupport[2] + sideSupport[3];
			sideChances[5] = sideSupport[4] + sideSupport[0] + sideSupport[1] + sideSupport[2] + sideSupport[3];

			if (initialDirection != ForgeDirection.UNKNOWN)
			{
				int ordinal = initialDirection.ordinal();
				sideChances[ordinal] += 4;
			}
			else
			{
				// Initial node
				for (int i = 0; i < sideChances.length; i++)
				{
					sideChances[i] += 2;
				}
			}

			sideChances[random.nextInt(sideChances.length)] += 2;

			for (ForgeDirection direction : ForgeDirection.VALID_DIRECTIONS)
			{
				int ordinal = direction.ordinal();
				if (openSides[ordinal])
				{
					int chance = sideChances[ordinal];
					if (chance > 0 && random.nextInt(1 + 2 * sideChances[ordinal]) != 0)
					{
						placements.add(sideVecs[ordinal]);
						heads.put(sideVecs[ordinal], new PipeHead(sideVecs[ordinal], direction));
					}
				}
			}
		}
	}

	protected PipeType getRandomPipeType(Random random)
	{
		PipeType[] pipeTypes = GFAPI.registry.getRegisteredPipeTypes();
		return pipeTypes[random.nextInt(pipeTypes.length)];
	}

	@Override
	public boolean generate(World world, Random random, int x, int y, int z)
	{
		if (world.getBlock(x, y, z) != Blocks.air || random.nextInt(8) != 0)
			return false;

		PipeType pipeType = getRandomPipeType(random);
		HashSet<KeyVec> pipePlacements = new HashSet<KeyVec>();
		HashMap<KeyVec, PipeHead> pipeHeads = new HashMap<KeyVec, PipeHead>();

		KeyVec initialNode = new KeyVec(x, y, z);
		pipePlacements.add(initialNode);
		pipeHeads.put(initialNode, new PipeHead(initialNode, ForgeDirection.UNKNOWN));

		for (int i = 0; i < 12; i++)
		{
			ArrayList<PipeHead> currentPipeHeads = new ArrayList<PipeHead>(pipeHeads.values());
			pipeHeads.clear();

			for (PipeHead pipeHead : currentPipeHeads)
			{
				pipeHead.branch(world, random, pipePlacements, pipeHeads);
			}
		}

		if (pipePlacements.size() > 6)
		{
			for (KeyVec pipe : pipePlacements)
			{
				if (random.nextInt(12) != 0)
				{
					GFAPI.implementation.placePipe(world, pipe.x, pipe.y, pipe.z, pipeType, GFAPI.gasTypeAir);
				}
			}
		}

		return true;
	}
}
