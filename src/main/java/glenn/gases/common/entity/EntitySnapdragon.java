package glenn.gases.common.entity;

import glenn.gasesframework.api.ExtendedGasEffectsBase;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntitySnapdragon extends EntityThrowable
{
	public EntitySnapdragon(World par1World)
	{
		super(par1World);
	}

	public EntitySnapdragon(World par1World, EntityLivingBase par2EntityLivingBase)
	{
		super(par1World, par2EntityLivingBase);
	}

	public EntitySnapdragon(World par1World, double par2, double par4, double par6)
	{
		super(par1World, par2, par4, par6);
	}

	/**
	 * Called when this EntityThrowable hits a block or entity.
	 */
	protected void onImpact(MovingObjectPosition par1MovingObjectPosition)
	{

		if (!worldObj.isRemote)
		{
			explode();
			setDead();
		}
	}

	private void explode()
	{
		this.worldObj.playSoundEffect(this.posX, this.posY, this.posZ, "random.explode", 1.0F, (1.0F + (this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat()) * 0.2F) * 4.0F);

		double size = 10.0D;
		double sizeSquared = size * size;
		List<Entity> list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, AxisAlignedBB.getBoundingBox(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size));
		for (Entity e : list)
		{
			if (e instanceof EntityLivingBase)
			{
				EntityLivingBase entity = (EntityLivingBase) e;

				double dx = entity.posX - posX;
				double dy = entity.posY - posY;
				double dz = entity.posZ - posZ;
				double d = dx * dx + dy * dy + dz * dz;
				if (d < sizeSquared)
				{
					Vec3 vec1 = Vec3.createVectorHelper(posX, posY, posZ);
					Vec3 vec2 = Vec3.createVectorHelper(entity.posX, entity.posY, entity.posZ);
					MovingObjectPosition movingObjectPosition = this.worldObj.func_147447_a(vec1, vec2, false, true, false);

					if (movingObjectPosition == null)
					{
						double d2 = (size - MathHelper.sqrt_double(d)) / size;

						if (d2 > 0.6D)
						{
							ExtendedGasEffectsBase gasEffects = ExtendedGasEffectsBase.get(entity);
							gasEffects.increment(ExtendedGasEffectsBase.EffectType.BLINDNESS, (int) (600.0D * d2 * d2 * d2));
							gasEffects.increment(ExtendedGasEffectsBase.EffectType.SLOWNESS, (int) (1400.0D * d2 * d2 * d2));
							entity.attackEntityFrom(DamageSource.causeMobDamage(getThrower()), (float) d2 * 4.0F);
						}
					}
				}
			}
		}

		worldObj.spawnEntityInWorld(new EntityFlashFX(worldObj, posX, posY, posZ));

		int x = MathHelper.floor_double(posX);
		int y = MathHelper.floor_double(posY);
		int z = MathHelper.floor_double(posZ);

		if (worldObj.getBlock(x, y, z).getMaterial().isReplaceable())
		{
			worldObj.setBlock(x, y, z, Blocks.fire);
		}
	}
}