package glenn.gases.common.entity;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class EntityFX extends Entity
{
	protected int lifetime;
	protected int maxLifetime;

	public EntityFX(World world)
	{
		super(world);
		setSize(0.2F, 0.2F);
		yOffset = height / 2.0F;
		isImmuneToFire = true;
	}

	public EntityFX(World world, double x, double y, double z)
	{
		this(world);
		setPosition(x, y, z);
	}

	@Override
	protected void entityInit()
	{

	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound)
	{
		lifetime = compound.getInteger("lifetime");
		maxLifetime = compound.getInteger("maxLifetime");
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound)
	{
		compound.setInteger("lifetime", lifetime);
		compound.setInteger("maxLifetime", maxLifetime);
	}

	@SideOnly(Side.CLIENT)
	public float getShadowSize()
	{
		return 0.0F;
	}

	@Override
	public boolean handleWaterMovement()
	{
		if (super.handleWaterMovement())
		{
			this.setDead();
		}
		return false;
	}

	@Override
	public boolean handleLavaMovement()
	{
		if (super.handleLavaMovement())
		{
			this.setDead();
		}
		return false;
	}

	@Override
	public boolean isEntityInvulnerable()
	{
		return true;
	}

	@Override
	protected void func_145780_a(int p_145780_1_, int p_145780_2_, int p_145780_3_, Block p_145780_4_)
	{

	}

	@Override
	protected boolean canTriggerWalking()
	{
		return false;
	}

	public void onUpdate()
	{
		if (++lifetime >= maxLifetime && !worldObj.isRemote)
		{
			setDead();
		}

		this.lastTickPosX = this.posX;
		this.lastTickPosY = this.posY;
		this.lastTickPosZ = this.posZ;
		super.onUpdate();

		moveEntity(motionX, motionY, motionZ);
	}

	public abstract double getScale(float partialTick);

	public abstract ResourceLocation getTexture();

	public int getColor()
	{
		return 0xFFFFFFFF;
	}

	public void render(Tessellator tessellator, RenderManager renderManager, float partialTick, double x, double y, double z)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x, (float) y, (float) z);
		GL11.glRotatef(180.0F - renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-renderManager.playerViewX, 1.0F, 0.0F, 0.0F);

		double d = getScale(partialTick);

		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		tessellator.addVertexWithUV(-d, -d, 0.0D, 0.0D, 0.0D);
		tessellator.addVertexWithUV(d, -d, 0.0D, 1.0D, 0.0D);
		tessellator.addVertexWithUV(d, d, 0.0D, 1.0D, 1.0D);
		tessellator.addVertexWithUV(-d, d, 0.0D, 0.0D, 1.0D);
		tessellator.draw();

		GL11.glPopMatrix();
	}
}