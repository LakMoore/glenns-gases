package glenn.gases.common.block;

import glenn.gases.Gases;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.block.IGasSource;
import glenn.gasesframework.api.gastype.GasType;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

/**
 * A modified Bedrock block that emits Void gas. Completely overrides regular
 * bedrock block.
 */
public class BlockBedrock extends Block implements IGasSource
{
	/** Creates a new, modified Bedrock block that will emit void gas. */
	public BlockBedrock(Material material)
	{
		super(material);
		this.setTickRandomly(true);
	}

	/** Updates bedrock block (emit void gas) */
	@Override
	public void updateTick(World world, int x, int y, int z, Random random)
	{
		if (world.provider.dimensionId == 0)
		{
			ForgeDirection side = ForgeDirection.VALID_DIRECTIONS[random.nextInt(6)];
			int x1 = x + side.offsetX;
			int y1 = y + side.offsetY;
			int z1 = z + side.offsetZ;

			if (Gases.gasTypeVoid.getDissipation(world, x1, y1, z1, random) > 0 | y1 >= Gases.configurations.gases.voidGas.maxHeight)
			{
				return;
			}

			GFAPI.implementation.tryFillWithGas(world, random, x1, y1, z1, Gases.gasTypeVoid);
		}
	}

	@Override
	public GasType getGasTypeFromSide(World world, int x, int y, int z, ForgeDirection side)
	{
		return Gases.gasTypeVoid;
	}

	@Override
	public GasType extractGasTypeFromSide(World world, int x, int y, int z, ForgeDirection side)
	{
		return Gases.gasTypeVoid;
	}

	@Override
	public boolean connectToPipe(IBlockAccess blockaccess, int x, int y, int z, ForgeDirection side)
	{
		return false;
	}
}
