package glenn.gases.common.block;

import java.util.Random;

import glenn.gases.Gases;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.block.IGasSource;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

/**
 * A modified Bedrock block that emits Whispering Fog gas. Replaces regular
 * bedrock blocks at world generation.
 */
public class BlockFogEmitter extends Block
{
	/**
	 * Creates a new, bedrock-with-a-hole block that will emit whispering fog
	 * gas.
	 */
	public BlockFogEmitter(Material material)
	{
		super(material);
	}

	/** Updates cracked bedrock block (emit whispering fog gas) */
	@Override
	public void updateTick(World world, int x, int y, int z, Random random)
	{
		ForgeDirection side = ForgeDirection.VALID_DIRECTIONS[random.nextInt(6)];
		int x1 = x + side.offsetX;
		int y1 = y + side.offsetY;
		int z1 = z + side.offsetZ;
		GasType g = GFAPI.implementation.getGasType(world, x1, y1, z1);
		if (g == Gases.gasTypeWhisperingFog)
		{
			int volume = GFAPI.implementation.getGasVolume(world, x1, y1, z1);
			if (volume > 0)
			{
				GFAPI.implementation.placeGas(world, x1, y1, z1, Gases.gasTypeWhisperingFog, 1 + volume);
			}
		}
		else if (world.isAirBlock(x1, y1, z1))
		{
			GFAPI.implementation.placeGas(world, x1, y1, z1, Gases.gasTypeWhisperingFog, 1);
		}
		world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
	}

	@Override
	public int tickRate(World world)
	{
		return 50;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z)
	{
		world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
	}
}
